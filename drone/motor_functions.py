# Lo scopo di questo script e' di fornire un algoritmo che,
# dati i valori di due assi di rotazione, restituisca la potenza 
# da attribuire ad ogni motore.
# L'implementazione di questo algoritmo si e' resa necessaria in quanto
# per la versione 2.0 del progetto, il drone verra' controllato da due
# assi (nel nostro caso di un accellerometro, ma d'ora in avanti sara'
# possibile usare un qualsiasi controllo ad assi), mentre il vecchio
# metodo permetteva solo di usare il 100% della potenza dei motori.
# Riassumendo, grazie a questo algoritmo sara' possibile fornire
# ai motori un valore arbitrario di potenza che varia tra -100 e 100.

import math

def calculate_absolute_power(x_angle, y_angle):
	"""
	Calculates the absolute power for the motor.
	This means that we calculate the distance between
	the coordinate (0,0) and the intersection between
	the lines y = x_angle and x = y_angle
	"""
	x_angle_percent = x_angle / 90 * 100
	y_angle_percent = y_angle / 90 * 100

	intersection_point = (x_angle_percent, y_angle_percent)
	print(intersection_point)
	absolute_power = math.sqrt(math.pow(intersection_point[0], 2)\
	+ math.pow(intersection_point[1], 2))
	if(absolute_power > 100):
		absolute_power = 100
	return int(absolute_power)

def calculate_per_motor_power(x_angle, y_angle):
	"""
	Calculates the power to give each motor given the angle
	of two axis used to control the drone.

	"""
	result = [0,0]

	#arccos((P122 + P132 - P232) / (2 * P12 * P13))
	absolute_power = calculate_absolute_power(x_angle, y_angle)
	
	x_angle_percent = x_angle / 90 * 100
	y_angle_percent = y_angle / 90 * 100

	intersection_point = (x_angle_percent, y_angle_percent)

	#get the angle
	p13 = pdist((0,0), intersection_point)
	p23 = pdist(intersection_point, (0, 100))
	p12 = 100
	angle = math.acos((math.pow(p12, 2) + math.pow(p13, 2) \
	- math.pow(p23, 2)) / (2 * p12 * p13))

	#angle to degrees
	#x : angle = 180 : 3.14
	angle = int(180 / 3.14 * angle)
	#print(angle)

	if angle <= 90:
		ratio = (20 * angle - 900) / 9
		result[0] = absolute_power
		result[1] = int(absolute_power * ratio / 100)
	elif 90 < angle <= 180:
		ratio = (20 * angle - 2700) / (-9)
		result[0] = int(absolute_power * ratio / 100)
		result[1] = absolute_power

	if x_angle < 0:
		result[0] *= -1
		result[1] *= -1

	return result


def pdist(point1, point2):
	"""
	Returns the distance between two points.
	Accepts two tuples representing the point coordinates (x,y)
	"""
	result = math.sqrt(math.pow(point1[0] - point2[0], 2) \
	+ math.pow(point1[1] - point2[1], 2))
	return result

def test():
	import time
	starttime = time.clock()
	calculate_per_motor_power(45, 60)
	endtime = time.clock()
	print(endtime - starttime)

# x : 45 = 100 : 90

# < 90 9y = 20x - 900 
# > 90 -9y = 20x - 2700