command_bindings = {
	'up-left' : '090-90',
	'up-right' : '090090',
	'down-right' : '-90090',
	'down-left' : '-90-90',
	'down' : '-90000',
	'left' : '000-90',
	'up' : '090000',
	'right' : '000090',
	'stop' : '000000',
}