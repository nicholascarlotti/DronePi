import socket
import drone_movement

server_address = ("0.0.0.0", 4078)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

def start():
	sock.bind(server_address)
	#Read loop
	while(True):
		#Read the first 6 bytes of the next message
		incoming_data = sock.recv(6)
		drone_movement.set_motor_speed(int(incoming_data[:3]), int(incoming_data[3:]))

if __name__ == "__main__":
	try:
		start()
	except KeyboardInterrupt:
		sock.close()
		print('Closing...')