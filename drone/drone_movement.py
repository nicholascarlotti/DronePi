import motor_functions

#Pin declarations

leftmotor_input1 = 2
leftmotor_input2 = 3
leftmotor_enable = 4

rightmotor_input1 = 17
rightmotor_input2 = 27
rightmotor_enable = 22

rightmotor_power = 0
leftmotor_power = 0

try:
	import RPi.GPIO as GPIO
	GPIO.setmode(GPIO.BCM)
	GPIO.setwarnings(False)
	
	#Pin setup

	GPIO.setup(leftmotor_input1, GPIO.OUT)
	GPIO.setup(leftmotor_input2, GPIO.OUT)
	GPIO.setup(leftmotor_enable, GPIO.OUT)

	GPIO.setup(rightmotor_input1, GPIO.OUT)
	GPIO.setup(rightmotor_input2, GPIO.OUT)
	GPIO.setup(rightmotor_enable, GPIO.OUT)

	#Pwm pins declaration and setup

	lm_pwm = GPIO.PWM(leftmotor_enable, 50)
	rm_pwm = GPIO.PWM(rightmotor_enable, 50)
	lm_pwm.start(0)
	rm_pwm.start(0)

except ImportError:
	print('\033[93m=================')
	print('No GPIO module found, you are either not running this server on a RPi \nor have not installed the modules')
	print('================= \033[0m')


def set_motor_speed(x_axis_value, y_axis_value):
	
	if(x_axis_value == y_axis_value == 0):
		lm_pwm.ChangeDutyCycle(0)
		rm_pwm.ChangeDutyCycle(0)
		return

	motor_speed = motor_functions.calculate_per_motor_power(x_axis_value, y_axis_value)

	left_power = motor_speed[0]
	right_power = motor_speed[1]

	if left_power >= 0:
		#Standard motor power
		GPIO.output(leftmotor_input1, True)
		GPIO.output(leftmotor_input2, False)
	else:
		#Invert motor power
		GPIO.output(leftmotor_input1, False)
		GPIO.output(leftmotor_input2, True)

	if right_power >= 0:
		#Standard motor power
		GPIO.output(rightmotor_input1, True)
		GPIO.output(rightmotor_input2, False)
	else:
		#Invert motor power
		GPIO.output(rightmotor_input1, False)
		GPIO.output(rightmotor_input2, True)

	lm_pwm.ChangeDutyCycle(left_power)
	rm_pwm.ChangeDutyCycle(right_power)
